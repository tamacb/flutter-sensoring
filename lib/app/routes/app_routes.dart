part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const SENSORING = _Paths.SENSORING;
}

abstract class _Paths {
  static const HOME = '/home';
  static const SENSORING = '/sensoring';
}
