import 'package:get/get.dart';

import 'package:flutter_sensor/app/modules/home/bindings/home_binding.dart';
import 'package:flutter_sensor/app/modules/home/views/home_view.dart';
import 'package:flutter_sensor/app/modules/sensoring/bindings/sensoring_binding.dart';
import 'package:flutter_sensor/app/modules/sensoring/views/sensoring_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SENSORING;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.SENSORING,
      page: () => SensoringView(),
      binding: SensoringBinding(),
    ),
  ];
}
