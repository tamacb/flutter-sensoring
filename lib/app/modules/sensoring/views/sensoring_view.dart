import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/sensoring_controller.dart';

class SensoringView extends GetView<SensoringController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SplashView'),
        centerTitle: true,
      ),
      body: Obx(()=>Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Center(
            child: DecoratedBox(
              decoration: BoxDecoration(
                border: Border.all(width: 1.0, color: Colors.black38),
              ),
              child: SizedBox(
                height: controller.snakeRows.value * controller.snakeCellSize.value,
                width: controller.snakeColumns.value * controller.snakeCellSize.value,
                child: Column(
                  children: [
                    Text('${controller.snakeRows.value}'),
                    Text('${controller.snakeColumns.value}'),
                    Text('${controller.snakeCellSize.value}'),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Accelerometer: ${controller.resultAccelero()}'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('UserAccelerometer: ${controller.resultUserAccelero()}'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Gyroscope: ${controller.resultGyro()}'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Magnetometer: ${controller.resultMagneto()}'),
              ],
            ),
          ),
        ],
      )),
    );
  }
}
