import 'package:get/get.dart';

import '../controllers/sensoring_controller.dart';

class SensoringBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SensoringController>(
      () => SensoringController(),
    );
  }
}
