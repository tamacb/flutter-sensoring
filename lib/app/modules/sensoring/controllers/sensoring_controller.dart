import 'dart:async';
import 'package:get/get.dart';
import 'package:sensors_plus/sensors_plus.dart';

class SensoringController extends GetxController {
  final count = 0.obs;

  final snakeRows = 20.obs;
  final snakeColumns = 20.obs;
  final snakeCellSize = 10.0.obs;

  final accelerometerValues = <double>[].obs;
  final userAccelerometerValues = <double>[].obs;
  final gyroscopeValues = <double>[].obs;
  final magnetometerValues = <double>[].obs;
  final streamSubscriptions = <StreamSubscription<dynamic>>[].obs;

  @override
  void onInit() {
    super.onInit();
    streamSubscriptions.add(
      accelerometerEvents.listen(
        (AccelerometerEvent event) {
          accelerometerValues.value = <double>[event.x, event.y, event.z];
        },
      ),
    );

    streamSubscriptions.add(
      gyroscopeEvents.listen(
        (GyroscopeEvent event) {
          gyroscopeValues.value = <double>[event.x, event.y, event.z];
          gyroscopeValues.refresh();
        },
      ),
    );

    streamSubscriptions.add(
      userAccelerometerEvents.listen(
        (UserAccelerometerEvent event) {
          userAccelerometerValues.value = <double>[event.x, event.y, event.z];
        },
      ),
    );

    streamSubscriptions.add(
      magnetometerEvents.listen(
        (MagnetometerEvent event) {
          magnetometerValues.value = <double>[event.x, event.y, event.z];
        },
      ),
    );
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
    for (final subscription in streamSubscriptions) {
      subscription.cancel();
    }
  }

  resultGyro() => gyroscopeValues.map((double v) => v.toStringAsFixed(1)).toList();
  resultAccelero() => accelerometerValues.map((double v) => v.toStringAsFixed(1)).toList();
  resultMagneto() => magnetometerValues.map((double v) => v.toStringAsFixed(1)).toList();
  resultUserAccelero() => magnetometerValues.map((double v) => v.toStringAsFixed(1)).toList();

  void increment() => count.value++;
}
